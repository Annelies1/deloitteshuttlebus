//
//  VertrektijdenViewController.swift
//  DeloitteShuttlebus
//
//  Created by Annelies van Ees on 07/03/15.
//  Copyright (c) 2015 Annelies van Ees. All rights reserved.
//

import UIKit

class VertrektijdenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    let richtingRAInaarDeloitteSegmentNr = 0;
    let richtingDeloitteNaarRAISegmentNr = 1;
    
    var vertrektijdenRAInaarDeloitte = ["7:10", "7:15", "7:30", "7:45", "8:00", "8:10", "8:20", "8:30", "8:40", "8:50",
                                        "9:00", "9:10", "9:20", "9:30", "9:40", "9:50", "10:00", "10:15", "10:30",
                                        "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30"]
    var vertrektijdenDeloitteNaarRai = ["10:45", "11:15", "11:45", "12:15", "12:45", "13:15", "13:45", "14:15", "14:45",
                                        "15:15", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:10", "17:20",
                                        "17:30", "17:40", "17:50", "18:00", "18:10", "18:20", "18:30", "18:40", "18:50", "19:10", "19:30"]
    
    @IBOutlet var richtingSegmentControl: UISegmentedControl!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickedRichtingSegmentControl(sender: AnyObject) {
        self.tableView.reloadData()
    }
    
    func getSelectedRichtingArray() -> Array<String> {
        if self.richtingSegmentControl.selectedSegmentIndex == richtingDeloitteNaarRAISegmentNr {
            return vertrektijdenDeloitteNaarRai
        }else {
            return vertrektijdenRAInaarDeloitte
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var richtingArray = self.getSelectedRichtingArray()
        return richtingArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("prototypeCell") as UITableViewCell
        cell.textLabel?.text = self.getSelectedRichtingArray()[indexPath.row]
        return cell
    }

}
