//
//  MapsViewController.swift
//  DeloitteShuttlebus
//
//  Created by Annelies van Ees on 08/03/15.
//  Copyright (c) 2015 Annelies van Ees. All rights reserved.
//

import UIKit

class MapsViewController: UIViewController {

    @IBOutlet var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        
        var camera = GMSCameraPosition.cameraWithLatitude(52.339935, longitude: 4.875869, zoom: 14)
        mapView.camera = camera
        // Do any additional setup after loading the view.
        addMyLocationButton()
        showBusjesMarker()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addMyLocationButton() {
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
    }

    func showBusjesMarker() {
        var position = CLLocationCoordinate2DMake(52.336658, 4.862651)
        var marker = GMSMarker(position: position)
        marker.title = "Deloitte Busje"
        marker.map = mapView
    }
    
    
}
