//
//  ViewController.swift
//  DeloitteShuttlebus
//
//  Created by Annelies van Ees on 07/03/15.
//  Copyright (c) 2015 Annelies van Ees. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var vertrektijdenButton: UIButton!
    @IBOutlet var weigerdagenButton: UIButton!
    @IBOutlet var locatieBusjeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        vertrektijdenButton.layer.cornerRadius = 10;
        weigerdagenButton.layer.cornerRadius = 10;
        locatieBusjeButton.layer.cornerRadius = 10;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

